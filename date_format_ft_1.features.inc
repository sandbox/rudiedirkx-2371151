<?php

/**
 * Implements hook_features_export_options().
 */
function date_format_features_export_options() {
  $query = db_select('date_format_type', 'f');
  $query->fields('f', array('type', 'title'));
  $query->orderBy('f.type');
  $formats = $query->execute()->fetchAllKeyed(0, 1);
  return $formats;
}

/**
 * Implements hook_features_export().
 */
function date_format_features_export($data, &$export, $module_name = '') {
  $pipe = array();

  $export['dependencies']['features'] = 'features';

  foreach ($data as $component) {
    $export['features']['date_format'][$component] = $component;
    $export['features']['variable']['date_format_' . $component] = 'date_format_' . $component;
  }

  return $pipe;
}

/**
 * Implements hook_features_export_render().
 */
function date_format_features_export_render($module, $data) {
  $params = array(':types' => $data);

  $raw_date_formats = db_query("SELECT format, type FROM {date_formats} WHERE type = 'custom' ORDER BY type, format")->fetchAll();
  $date_formats = array();
  foreach ($raw_date_formats as $df) {
    $date_formats[ $df->type . '-' . $df->format ] = $df;
  }
  ksort($date_formats);

  $raw_date_format_type = db_query('SELECT type, title FROM {date_format_type} WHERE type IN (:types) ORDER BY type', $params)->fetchAll();
  $date_format_type = array();
  foreach ($raw_date_format_type as $df) {
    $date_format_type[ $df->type ] = $df;
  }
  ksort($date_format_type);

  $code = array(
    'date_formats' => $date_formats,
    'date_format_locale' => array(),
    'date_format_type' => $date_format_type,
  );

  if (module_exists('locale')) {
    $raw_date_format_locale = db_query('SELECT format, type, language FROM {date_format_locale} WHERE type IN (:types) ORDER BY type, format', $params)->fetchAll();
    $date_format_locale = array();
    foreach ($raw_date_format_locale as $df) {
      $date_format_locale[ $df->type . '-' . $df->language ] = $df;
    }
    ksort($date_format_locale);

    $code['date_format_locale'] = $date_format_locale;
  }

  $code = "  return " . features_var_export($code, '  ') . ";";

  return array('date_format_features_default' => $code);
}

/**
 * Implements hook_features_revert().
 */
function date_format_features_revert($module) {
  date_format_features_rebuild($module);
}

/**
 * Implements hook_features_rebuild().
 *
 * Wow, date formats are so weird and redundant and weird...
 */
function date_format_features_rebuild($module) {
  $formats = module_invoke($module, 'date_format_features_default');

  // Literally copy date_formats to db.
  foreach ($formats['date_formats'] as $format) {
    db_merge('date_formats')->key($format)->execute();
  }

  // Literally copy date_format_type to db.
  $intersect = array_flip(array('type'));
  foreach ($formats['date_format_type'] as $format) {
    $key = array_intersect_key($format, $intersect);
    db_merge('date_format_type')->key($key)->fields($format)->execute();
  }

  // Copy & overwrite date_format_locale to the db IF locale.module is enabled.
  if (module_exists('locale')) {
    $intersect = array_flip(array('type', 'language'));
    foreach ($formats['date_format_locale'] as $format) {
      $key = array_intersect_key($format, $intersect);
      db_merge('date_format_locale')->key($key)->fields($format)->execute();
    }
  }
}
