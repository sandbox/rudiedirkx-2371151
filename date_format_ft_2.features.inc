<?php

/**
 * Implements hook_features_export_options().
 */
function date_format_2_features_export_options() {
  $query = db_select('date_format_type', 'f');
  $query->fields('f', array('type', 'title'));
  $query->orderBy('f.type');
  $formats = $query->execute()->fetchAllKeyed(0, 1);
  return $formats;
}

/**
 * Implements hook_features_export().
 */
function date_format_2_features_export($data, &$export, $module_name = '') {
  $export['dependencies']['features'] = 'features';
  $export['dependencies']['date_format_ft'] = 'date_format_ft';

  // Locale is a dependency if it's enabled AND specific date formats have been defined.
  if (module_exists('locale')) {
    if (db_query('SELECT COUNT(1) FROM {date_format_locale}')->fetchField()) {
      $export['dependencies']['locale'] = 'locale';
    }
  }

  $export['features']['date_format_2'] = drupal_map_assoc($data);

  return array();
}

/**
 * Implements hook_features_export_render().
 */
function date_format_2_features_export_render($module, $data) {
  $params = array(':types' => $data);

  // Relevant types from db.
  $system_types = system_date_format_types();
  $formats = db_query('SELECT type, title FROM {date_format_type} WHERE type IN (:types) ORDER BY type', $params)->fetchAllAssoc('type', PDO::FETCH_ASSOC);

  // Add default (unlocalized) format.
  foreach ($formats as $name => $foo) {
    $formats[$name]['formats']['und'] = variable_get('date_format_' . $name);

    if (isset($system_types[$name])) {
      unset($formats[$name]['title']);
    }
  }

  // Add localized formats.
  if (module_exists('locale')) {
    $localized = db_query('SELECT format, type, language FROM {date_format_locale} WHERE type IN (:types)', $params);
    foreach ($localized as $format) {
      $formats[ $format->type ]['formats'][ $format->language ] = $format->format;
    }
  }

  // Sort it like Features does, so the code order equals the diff order.
  foreach ($formats as $name => $foo) {
    ksort($formats[$name]['formats']);
    ksort($formats[$name]);
  }

  $code = "  return " . features_var_export($formats, '  ') . ";";
  return array('date_format_2_features_default' => $code);
}

/**
 * Implements hook_features_revert().
 */
function date_format_2_features_revert($module) {
  date_format_2_features_rebuild($module);
}

/**
 * Implements hook_features_rebuild().
 */
function date_format_2_features_rebuild($module) {
  $formats = module_invoke($module, 'date_format_2_features_default');

  $db_formats = db_query('SELECT format, 1 FROM {date_formats}')->fetchAllKeyed();

  $has_locale = module_exists('locale');

  foreach ($formats as $type => $format) {
    $key = compact('type');

    // Create/update type.
    if (isset($format['title'])) {
      $data = array('title' => $format['title'], 'locked' => 0);
      db_merge('date_format_type')->key($key)->fields($data)->execute();
    }

    foreach ($format['formats'] as $language => $date) {
      // For some reason, they have to exist in the db as standalone format too. Go redundancy!
      if (!isset($db_formats[$date])) {
        $data = array('format' => $date, 'type' => 'custom', 'locked' => 0);
        db_insert('date_formats')->fields($data)->execute();
        $db_formats[$date] = 1;
      }

      // The default (unlocalized) formats, stored in vars.
      if ($language == 'und') {
        variable_set('date_format_' . $type, $date);
      }
      // Localized formats, stored in a locale table.
      else {
        if ($has_locale) {
          $local_ley = $key + compact('language');
          $data = array('format' => $date);
          db_merge('date_format_locale')->key($local_ley)->fields($data)->execute();
        }
      }
    }
  }
}
